from setuptools import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='wcy_timetable_downloader',
    version='0.4.4',
    packages=['wcy_timetable_downloader'],
    url='https://gitlab.com/mackgr/wcy-timetable-downloader',
    license='MIT',
    author='Maciej Grabowski',
    author_email='',
    setup_requires=['wheel'],
    install_requires=requirements,
    entry_points={
        'console_scripts': [
            'update-calendar=wcy_timetable_downloader.cli:update_calendar',
            'clear-secrets=wcy_timetable_downloader.cli:clear_secrets',
            'configure=wcy_timetable_downloader.cli:configure'
        ],
    },
    include_package_data=True,
    description='Package to update your calendar with events from WCY E-Dziekanat system.'
)
