import abc
from typing import Dict, List


class CalendarService(abc.ABC):
    @abc.abstractmethod
    def get_future_events(self) -> List[Dict]:
        pass

    @abc.abstractmethod
    def get_events(self) -> List[Dict]:
        pass

    @abc.abstractmethod
    def insert_event(self, event: Dict) -> Dict:
        pass

    @abc.abstractmethod
    def update_event(self, event_id: str, event: Dict) -> Dict:
        pass

    @abc.abstractmethod
    def delete_event(self, event_id: str):
        pass

    @abc.abstractmethod
    def patch_event(self, event_id: str, event: Dict) -> Dict:
        pass
