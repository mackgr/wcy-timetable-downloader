import datetime as dt
from typing import Tuple, List, Iterable, Optional

from wcy_timetable_downloader.calendar_service import CalendarService
from wcy_timetable_downloader.timetable import (
    Timetable,
    merge_schedules,
    semester_to_months,
    date_to_semester,
)
from wcy_timetable_downloader.timetable_service import TimetableService


class CalendarUpdater:
    def __init__(
        self,
        calendar_api: CalendarService,
        timetable_service: TimetableService,
        groups: List,
    ):
        self.calendar_api = calendar_api
        self.timetable_service = timetable_service
        self.old_events = self.calendar_api.get_events()
        self.old_timetable = Timetable.from_calendar_events(self.old_events)

        timetables = {}
        for group in groups:
            name = group["name"]
            timetables_by_semester = []
            for semester in timetable_service.neighboring_semesters(dt.date.today()):
                timetable = Timetable.from_csv_string(
                    self.timetable_service.download_timetable(name, semester)
                )
                timetables_by_semester.append(timetable)
            group_timetable = merge_schedules(timetables_by_semester)
            timetables[name] = group_timetable
        self.new_timetable = merge_schedules(timetables.values())

    def changes_detected(self) -> bool:
        return self.old_timetable != self.new_timetable

    def update(self) -> Tuple[List, List, List]:
        event_names_to_change = self.old_timetable.changed_event_names(
            self.new_timetable
        )
        events_to_change = self.new_timetable.filter_by_names(
            event_names_to_change
        ).to_calendar_events()
        changed_event_names = self.change_events(events_to_change)

        event_names_to_delete = self.old_timetable.removed_event_names(
            self.new_timetable
        )
        deleted_event_names = self.delete_events(event_names_to_delete)

        event_names_to_add = self.old_timetable.added_event_names(self.new_timetable)
        events_to_add = self.new_timetable.filter_by_names(
            event_names_to_add
        ).to_calendar_events()
        added_event_names = self.add_events(events_to_add)

        return changed_event_names, deleted_event_names, added_event_names

    def add_events(self, events_to_add: Iterable) -> List:
        added_event_names = []
        for event in events_to_add:
            inserted_event = self.calendar_api.insert_event(event)
            added_event_names.append(inserted_event["summary"])
        return added_event_names

    def delete_events(self, event_names_to_delete: Iterable) -> List:
        deleted_event_names = []
        for event_tuple in event_names_to_delete:
            event_id = self.event_id(event_tuple[0], event_tuple[1])
            self.calendar_api.delete_event(event_id)
            deleted_event_names.append(event_tuple[0])
        return deleted_event_names

    def change_events(self, events_to_change: Iterable) -> List:
        changed_event_names = []
        for event in events_to_change:
            event_semester = date_to_semester(
                dt.datetime.strptime(event["start"]["dateTime"], "%Y-%m-%dT%H:%M:%S")
            )
            event_id = self.event_id(event["summary"], event_semester)
            changed_event = self.calendar_api.patch_event(event_id, event)
            changed_event_names.append(changed_event["summary"])
        return changed_event_names

    def event_id(self, event_name: str, event_semester: str) -> Optional[str]:
        months = semester_to_months(event_semester)
        filtered_events = list(
            filter(
                lambda event: event["summary"] == event_name
                and dt.datetime.strptime(
                    event["start"]["dateTime"], "%Y-%m-%dT%H:%M:%S%z"
                ).month
                in months,
                self.old_events,
            )
        )
        return filtered_events[0]["id"] if filtered_events else None
