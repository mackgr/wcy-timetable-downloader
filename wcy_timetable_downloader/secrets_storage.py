import pickle

from cryptography.fernet import Fernet
from keyring.errors import PasswordDeleteError
from keyrings.cryptfile.cryptfile import CryptFileKeyring


class SecretsStorage:
    def __init__(self, service_id, encryption_key):
        self.service_id = service_id
        self.cipher_suite = Fernet(encryption_key)
        self.keyring = CryptFileKeyring()
        self.keyring.keyring_key = encryption_key.decode("ascii")

    def set(self, key, value):
        value_bytes = pickle.dumps(value)
        obfuscated_value = self.obfuscate(value_bytes).hex()
        self.keyring.set_password(self.service_id, key, obfuscated_value)

    def get(self, key):
        hexed_obfuscated_value = self.keyring.get_password(self.service_id, key)
        if hexed_obfuscated_value:
            obfuscated_value = bytes.fromhex(hexed_obfuscated_value)
            deobfuscated_value = self.deobfuscate(obfuscated_value)
            value = pickle.loads(deobfuscated_value)
            return value
        else:
            return None

    def delete(self, key):
        try:
            self.keyring.delete_password(self.service_id, key)
        except PasswordDeleteError:
            pass

    def obfuscate(self, text: bytes) -> bytes:
        return self.cipher_suite.encrypt(text)

    def deobfuscate(self, text: bytes) -> str:
        return self.cipher_suite.decrypt(text)
