import datetime as dt
import re
from pathlib import Path
from typing import List, Set, Dict

import calmjs.parse.asttypes
import cert_human
import requests
from bs4 import BeautifulSoup
from calmjs.parse import es5
from fake_useragent import UserAgent
import os


class SemesterName:
    winter = 'zimowy'
    summer = 'letni'
    session = 'sesja poprawkowa'


class TimetableService(object):
    def __init__(self, login_url: str):
        self.cert_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'certs.pem')
        self.login_url = login_url

        self.refresh_certs()
        self.session = requests.Session()
        self.session.verify = self.cert_path

        self.set_headers()
        self.sid = self.get_sid()
        self.url_after_login = None

    def log_in(self, username: str, password: str):
        payload = {
            "userid": username,
            "password": password,
            "default_fun": 1,
            "formname": 'login',
            "view_height": 700,
            "view_width": 1120
        }
        response = self.session.post(self.login_url, data=payload, params={'sid': self.sid})
        self.url_after_login = response.url

    def get_semester_codes(self) -> Dict:
        menu_script = self.get_js_scripts()[2]
        menu_var = re.search(r'=(.*?)-->', menu_script, re.DOTALL).group(1).strip()[:-1]
        js_tree = es5(menu_var)
        timetable_menu_array = js_tree.children()[0].expr.items[2].items[7]

        semesters = {}
        for item in timetable_menu_array:
            if isinstance(item, calmjs.parse.asttypes.Array):
                semester_name = item.items[1].value.strip('\'')
                semester_code = int(re.search(r'\((.*?)\)', item.items[2].value).group(1).split(',')[1])
                semesters[semester_name] = semester_code
        return semesters

    def get_js_scripts(self) -> List:
        main_html = self.session.get(self.url_after_login, timeout=None).text
        soup = BeautifulSoup(main_html, "html.parser")
        return [script.text for script in soup.find_all('script')]

    def refresh_certs(self):
        certs = cert_human.CertChainStore.from_path(self.cert_path).certs if Path(self.cert_path).exists() else None
        if not certs or any(cert for cert in certs if cert.is_expired):
            cert_chain = self.download_certs()
            cert_chain.to_path(self.cert_path, overwrite=True)

    def download_certs(self) -> cert_human.CertChainStore:
        ca_root_cert = requests.get("https://www.certum.pl/CTNCA.pem").text
        ca_intermediate_cert = requests.get("https://repository.certum.pl/ovcasha2.pem").text
        wcy_cert = cert_human.CertStore.from_request(host=self.login_url).pem
        pem_string = ca_root_cert + ca_intermediate_cert + wcy_cert
        cert_chain = cert_human.CertChainStore.from_pem(pem_string)
        return cert_chain

    def get_sid(self) -> str:
        login_html = self.session.get(self.login_url)
        soup = BeautifulSoup(login_html.text, "html.parser")
        tag = soup.form
        url = tag['action']
        sid = url[14:]
        return sid

    def download_timetable(self, group: str, semester_name: str) -> str:
        semester_code = self.get_semester_codes()[semester_name]
        params = {
            'exv': group,
            'iid': semester_code,
            'mid': 328,
            'pos': 0,
            'rdo': 1
        }
        schedule_response = self.session.get(self.url_after_login, params=params, timeout=None)
        result = self.session.get(schedule_response.url, params={'opr': 'DTXT'}, timeout=None)
        return str(result.content, 'Windows-1250')

    def set_headers(self):
        ua = UserAgent()
        headers = {'User-Agent': str(ua.chrome)}
        self.session.headers.update(headers)

    def neighboring_semesters(self, date: dt.date) -> Set[str]:
        border_months = {2, 7, 9}
        semesters = set()
        semester = self.date_to_semester(date)
        semesters.add(semester)
        if date.month in border_months:
            next_semester = self.semester_successor(semester)
            semesters.add(next_semester)
        return semesters

    @staticmethod
    def date_to_semester(date: dt.date) -> str:
        winter_sem_months_old_year = {10, 11, 12}
        winter_sem_months_new_year = {1, 2}
        summer_sem_months = {3, 4, 5, 6, 7}

        if date.month in winter_sem_months_old_year:
            academic_year = "{0}/{1}".format(date.year, date.year + 1)
            semester_name = SemesterName.winter
        elif date.month in winter_sem_months_new_year:
            academic_year = "{0}/{1}".format(date.year - 1, date.year)
            semester_name = SemesterName.winter
        elif date.month in summer_sem_months:
            academic_year = "{0}/{1}".format(date.year - 1, date.year)
            semester_name = SemesterName.summer
        else:
            academic_year = "{0}/{1}".format(date.year - 1, date.year)
            semester_name = SemesterName.session
        return "{0} {1}".format(academic_year, semester_name)

    @staticmethod
    def semester_successor(semester: str) -> str:
        academic_year, semester_name = semester.split(' ')
        if semester_name == SemesterName.winter:
            return "{0} {1}".format(academic_year, SemesterName.summer)
        elif semester_name == SemesterName.summer:
            return "{0} {1}".format(academic_year, SemesterName.session)
        elif semester_name == SemesterName.session:
            first_year, second_year = academic_year.split('/')
            return '{0}/{1} {2}'.format(int(second_year), int(second_year) + 1, SemesterName.winter)
