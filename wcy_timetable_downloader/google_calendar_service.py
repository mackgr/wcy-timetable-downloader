import datetime
import pickle
from typing import Dict

from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build

from wcy_timetable_downloader.calendar_service import CalendarService
from wcy_timetable_downloader.secrets_storage import SecretsStorage


class GoogleCalendarService(CalendarService):
    def __init__(self, secrets_storage: SecretsStorage):
        self.scopes = ["https://www.googleapis.com/auth/calendar"]
        self.calendar_id = None
        self.secrets_storage = secrets_storage
        self.service = None
        self.events = None

    def authorization(self, credentials: Dict):
        if self.secrets_storage.get("token"):
            token = pickle.loads(self.secrets_storage.get("token"))
        else:
            token = None

        if not token or not token.valid:
            if token and token.expired and token.refresh_token:
                token.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_config(credentials, self.scopes)
                token = flow.run_console()
            self.secrets_storage.set("token", pickle.dumps(token))
        self.service = build("calendar", "v3", credentials=token)
        self.events = self.service.events()

    def create_calendar(self, name: str) -> str:
        calendar = {"summary": name, "timeZone": "Europe/Warsaw"}
        created_calendar = self.service.calendars().insert(body=calendar).execute()
        return created_calendar["id"]

    def get_future_events(self):
        now = datetime.datetime.utcnow().isoformat() + "Z"
        events_result = self.events.list(
            calendarId=self.calendar_id,
            timeMin=now,
            singleEvents=True,
            orderBy="startTime",
        ).execute()
        return events_result.get("items", [])

    def get_events(self):
        events_result = self.events.list(
            calendarId=self.calendar_id, singleEvents=True, orderBy="startTime"
        ).execute()
        return events_result.get("items", [])

    def insert_event(self, event):
        created_event = self.events.insert(
            calendarId=self.calendar_id, body=event
        ).execute()
        return dict(created_event)

    def update_event(self, event_id, event):
        return self.events.update(
            calendarId=self.calendar_id, eventId=event_id, body=event
        ).execute()

    def patch_event(self, event_id, event):
        response = self.events.patch(
            calendarId=self.calendar_id, eventId=event_id, body=event
        ).execute()
        return response

    def delete_event(self, event_id):
        return self.events.delete(
            calendarId=self.calendar_id, eventId=event_id
        ).execute()
