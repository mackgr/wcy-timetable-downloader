import datetime as dt
from io import StringIO

import pandas as pd


class TimetableCols:
    TOPIC = "Temat"
    LOCATION = "Lokalizacja"
    START_DATE = "Data rozpoczęcia"
    START_TIME = "Czas rozpoczęcia"
    END_DATE = "Data zakończenia"
    END_TIME = "Czas zakończenia"


class Timetable(object):
    def __init__(self, data: pd.DataFrame):
        cols = [
            TimetableCols.TOPIC,
            TimetableCols.LOCATION,
            TimetableCols.START_DATE,
            TimetableCols.START_TIME,
            TimetableCols.END_DATE,
            TimetableCols.END_TIME,
        ]
        if data.empty:
            self.data = pd.DataFrame(columns=cols)
        else:
            self.data = (
                data[cols]
                .sort_values(
                    [
                        TimetableCols.START_DATE,
                        TimetableCols.START_TIME,
                        TimetableCols.TOPIC,
                        TimetableCols.LOCATION,
                        TimetableCols.END_DATE,
                        TimetableCols.END_TIME,
                    ],
                    ascending=[True, True, True, True, True, True],
                )
                .drop_duplicates()
                .reset_index(drop=True)
            )

            self.data["semester"] = self.data.apply(
                lambda row: date_to_semester(
                    dt.date.fromisoformat(row[TimetableCols.START_DATE])
                ),
                axis=1,
            )

    @classmethod
    def from_csv_string(cls, csv_string):
        try:
            data = pd.read_csv(StringIO(csv_string), sep=",")
        except Exception:
            raise ValueError("Invalid CSV string")
        return cls(data)

    @classmethod
    def from_csv_file(cls, fp):
        data = pd.read_csv(fp, sep=",")
        return cls(data)

    @classmethod
    def from_calendar_events(cls, events):
        events_to_convert = []
        for ev in events:
            start_datetime = dt.datetime.strptime(
                ev["start"]["dateTime"], "%Y-%m-%dT%H:%M:%S%z"
            )
            end_datetime = dt.datetime.strptime(
                ev["end"]["dateTime"], "%Y-%m-%dT%H:%M:%S%z"
            )
            event_to_convert = {
                TimetableCols.TOPIC: ev["summary"],
                TimetableCols.LOCATION: ev["location"],
                TimetableCols.START_DATE: start_datetime.date().isoformat(),
                TimetableCols.START_TIME: start_datetime.time().strftime("%H:%M"),
                TimetableCols.END_DATE: end_datetime.date().isoformat(),
                TimetableCols.END_TIME: end_datetime.time().strftime("%H:%M"),
            }
            events_to_convert.append(event_to_convert)
        data = pd.DataFrame(events_to_convert)
        return cls(data)

    def __eq__(self, other):
        return self.data.equals(other.data)

    def __add__(self, other):
        data = pd.concat([self.data, other.data], ignore_index=True)
        return Timetable(data)

    def __str__(self):
        return str(self.data)

    def events_since(self, date):
        events = self.data[self.data[TimetableCols.START_DATE] >= date]
        return events.to_csv()

    def filter_by_names(self, names):
        mask = self.data.apply(
            lambda row: (row[TimetableCols.TOPIC], row["semester"]) in names, axis=1
        )
        new_data = self.data[mask]
        # new_data = self.data[self.data[TimetableCols.TOPIC].isin(names)]
        return Timetable(new_data)

    def to_csv(self):
        return self.data.to_csv(index=False)

    def to_records(self):
        return self.data.to_dict("records")

    def to_calendar_events(self):
        timezone = "Europe/Warsaw"
        records = self.to_records()
        events = []
        for rec in records:
            start_date = dt.date.fromisoformat(rec[TimetableCols.START_DATE])
            start_time = dt.time.fromisoformat(rec[TimetableCols.START_TIME])
            start_datetime = dt.datetime.combine(start_date, start_time)

            end_date = dt.date.fromisoformat(rec[TimetableCols.END_DATE])
            end_time = dt.time.fromisoformat(rec[TimetableCols.END_TIME])
            end_datetime = dt.datetime.combine(end_date, end_time)

            new_event = {
                "summary": rec[TimetableCols.TOPIC],
                "location": rec[TimetableCols.LOCATION],
                "start": {
                    "dateTime": start_datetime.isoformat(),
                    "timeZone": timezone,
                },
                "end": {"dateTime": end_datetime.isoformat(), "timeZone": timezone,},
            }
            events.append(new_event)
        return events

    def save_as_csv(self, path):
        return self.data.to_csv(path, index=False)

    def added_event_names(self, other):
        diff = diff_schedules(self, other)[[TimetableCols.TOPIC, "semester", "_merge"]]
        changed = self.changed_event_names(other)
        added = set(
            diff[diff["_merge"] == "right_only"]
            .reset_index()[[TimetableCols.TOPIC, "semester"]]
            .itertuples(index=False, name=None)
        )
        return added - changed

    def removed_event_names(self, other):
        return other.added_event_names(self)

    def changed_event_names(self, other):
        diff = diff_schedules(self, other)[[TimetableCols.TOPIC, "semester", "_merge"]]
        grouped = diff.groupby([TimetableCols.TOPIC, "semester"]).count()
        return set(
            grouped[grouped["_merge"] == 2]
            .reset_index()[[TimetableCols.TOPIC, "semester"]]
            .itertuples(index=False, name=None)
        )


def merge_schedules(schedules):
    data = pd.concat([s.data for s in schedules], ignore_index=True)
    return Timetable(data)


def diff_schedules(left, right):
    merged = left.data.merge(right.data, indicator=True, how="outer")
    return merged[merged["_merge"] != "both"]


def date_to_semester(date: dt.date) -> str:
    winter_sem_months = {10, 11, 12, 1, 2}
    summer_sem_months = {3, 4, 5, 6, 7}
    if date.month in winter_sem_months:
        return "zima"
    elif date.month in summer_sem_months:
        return "lato"
    else:
        return "sesja"


def semester_to_months(semester: str) -> set:
    if semester == "zima":
        return {10, 11, 12, 1, 2}
    elif semester == "lato":
        return {3, 4, 5, 6, 7}
    else:
        return {8, 9}
