#!/usr/bin/env python

import datetime as dt
import json
import os
from pathlib import Path

import click as click
from cryptography.fernet import Fernet

from wcy_timetable_downloader.calendar_updater import CalendarUpdater
from wcy_timetable_downloader.google_calendar_service import GoogleCalendarService
from wcy_timetable_downloader.notification_service import NotificationService
from wcy_timetable_downloader.secrets_storage import SecretsStorage
from wcy_timetable_downloader.timetable_service import TimetableService

LOGIN_URL = "https://s1.wcy.wat.edu.pl/ed1/"
SERVICE_ID = "WCY_TIMETABLE_DOWNLOADER"
CONFIG_FILENAME = "config.json"
CONFIG_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), CONFIG_FILENAME)
STORED_SECRETS = {"username", "password", "token", "calendar_credentials"}


@click.command()
@click.option("--email")
def update_calendar(email):
    if not first_use():
        with Path(CONFIG_PATH).open() as f:
            config = json.load(f)
        secrets_storage = SecretsStorage(SERVICE_ID, config["key"].encode("ascii"))
        downloader = TimetableService(LOGIN_URL)
        downloader.log_in(
            secrets_storage.get("username"), secrets_storage.get("password")
        )
        secret = json.loads(secrets_storage.get("calendar_credentials"))

        calendar_api = GoogleCalendarService(secrets_storage)
        calendar_api.calendar_id = config["calendar_id"]
        calendar_api.authorization(secret)
        updater = CalendarUpdater(calendar_api, downloader, config["groups"])

        if updater.changes_detected():
            changed, deleted, added = updater.update()
            if changed:
                log("Changed events: " + str(changed))
            if deleted:
                log("Deleted events: " + str(deleted))
            if added:
                log("Added events: " + str(added))
            if email:
                msg = create_notification_msg(changed, deleted, added)
                NotificationService(email).send(msg)
        else:
            log("No changes in timetable.")
    else:
        log("Run configuration script first")


@click.command()
@click.option("--username", prompt="E-Dziekanat username")
@click.password_option("--password", prompt="E-Dziekanat password")
@click.option("--group-name", prompt="Group name")
@click.option("--calendar-id", prompt="Google calendar ID")
@click.option(
    "--calendar-credentials", prompt="Calendar credentials JSON")
def configure(username, password, group_name, calendar_id, calendar_credentials):
    key = Fernet.generate_key()
    secrets_storage = SecretsStorage(SERVICE_ID, key)

    secrets_storage.set("username", username)
    secrets_storage.set("password", password)
    secrets_storage.set("calendar_credentials", calendar_credentials)
    script_config = {
        "groups": [{"name": group_name, "filter_type": "all"}],
        "calendar_id": calendar_id,
        "key": key.decode("ascii"),
    }
    secrets_storage.delete("token")

    calendar_api = GoogleCalendarService(secrets_storage)
    calendar_api.authorization(json.loads(secrets_storage.get("calendar_credentials")))
    with open(CONFIG_PATH, "w+") as config_file:
        json.dump(script_config, config_file, indent=2)


@click.command(help="Clear secrets saved by script.")
def clear_secrets():
    secrets_storage = SecretsStorage(SERVICE_ID, Fernet.generate_key())
    os.remove(CONFIG_FILENAME)
    click.echo("config file deleted")
    for secret in STORED_SECRETS:
        secrets_storage.delete(secret)
        click.echo("{} deleted".format(secret))


def first_use() -> bool:
    return not Path(CONFIG_PATH).exists()


def create_notification_msg(changed, deleted, added):
    changed_msg = (
        "CHANGED EVENTS:\n{}\n".format("\n".join(event for event in changed))
        if changed
        else ""
    )
    deleted_msg = (
        "DELETED EVENTS:\n{}\n".format("\n".join(event for event in deleted))
        if deleted
        else ""
    )
    added_msg = (
        "ADDED EVENTS:\n{}\n".format("\n".join(event for event in added))
        if added
        else ""
    )
    return "{0}{1}{2}".format(changed_msg, deleted_msg, added_msg)


def log(msg):
    click.echo(dt.datetime.now().replace(microsecond=0).isoformat() + "|" + msg)
