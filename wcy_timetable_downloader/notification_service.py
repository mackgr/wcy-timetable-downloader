import smtplib
from email.message import EmailMessage
import socket
import getpass


class NotificationService:
    def __init__(self, email_address):
        self.email_address = email_address

    def send(self, msg):
        msg_to_send = EmailMessage()
        msg_to_send.set_content(msg)
        msg_to_send["From"] = "{0}@{1}".format(getpass.getuser(), socket.gethostname())
        msg_to_send["To"] = self.email_address
        msg_to_send["Subject"] = "CHANGES IN WCY TIMETABLE"
        smtp_server = smtplib.SMTP("localhost")
        smtp_server.send_message(msg_to_send)
        smtp_server.quit()
