# WCY Timetable Downloader

Automatically downloads the timetable from the E-Dziekanat system and sends it to the user's google calendar.
Best to use as a cronjob to periodically update the class schedule in calendar.

## Installation
* (1) Create new virtual environment:
    
     `python3 -m venv ~/wcy-timetable-downloader`
    
* (2) Activate environment:
    
    On macOS and Linux:
    
     `source ~/wcy-timetable-downloader/bin/activate`
    
* (3) Install project:

    `pip install git+https://gitlab.com/mackgr/wcy-timetable-downloader`
* (4) Deactivate environment:

    `deactivate`

## Configuration
### Obtaining client credentials
- (1) Open to Google APIs Console using https://console.developers.google.com/
- (2) Sign in using your Google account
- (3) Click on “ENABLE API AND SERVICES”
- (4) Type “Google Calendar" in the search bar
- (5) Click on Google Calendar API item and hit "ENABLE" button
- (6) Click on “Create” button and type the name of project, for example "Timetable Downloader"
- (7) Click on "Credentials", then "Create credentials",then select "OAuth Client ID",
then "Create Credentials"
- (8) Select "Application type" "Other", type any client name and click "Create"
- (9) Click on "Configure consent screen"
- (10) Type name of application
- (11) Click "Add scopes", select "../auth/calendar" and click "Add", then "Save"
- (12) Click "OK"
- (13) Select download icon next to Client ID column and save JSON file

### Obtaining google calendar ID
- (1) Go to https://calendar.google.com/calendar/, sign in
- (2) Click on plus sign near to "Other calendars" area on the left and select "Create new calendar"
- (3) On "My calendars" area on the left select the calendar you just created
- (4) Click the menu to the right of the chosen calendar name and select "Calendar settings".
- (5) Scroll down and find Calendar ID field.

### Running configuration script
- (1) Run in terminal:

    `~/wcy-timetable-downloader/bin/configure`
 - (2) Provide the required data (take calendar credentials from previously saved JSON file and delete it)

### Running script
Run in terminal:

`~/wcy-timetable-downloader/bin/update-calendar`
  
 or if you want to receive an email notification of changes 
 (this assumes that you have correctly configured SMTP server on the machine):
 
 `~/wcy-timetable-downloader/bin/update-calendar --email your_email_address`
  
  and you will probably need to change your spam filter settings to receive notifications from your machine.
 
### Deinstallation
- (1) Run in terminal:

    `~/wcy-timetable-downloader/bin/clear-secrets`
- (2) Delete installed project directory: ~/wcy-timetable-downloader