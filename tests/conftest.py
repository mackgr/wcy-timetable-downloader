import pytest
from wcy_timetable_downloader import timetable


@pytest.fixture
def shorter_schedule_csv_string():
    return (
        "Temat,Lokalizacja,Data rozpoczęcia,"
        "Czas rozpoczęcia,Data zakończenia,Czas zakończenia,"
        "Przypomnienie wł./wył.,Data przypomnienia,Czas przypomnienia\n"
        "Zarządzanie projektami (w) [1],316 S,"
        "2019-10-01,15:45,2019-10-01,17:20,"
        "Fałsz,2019-10-01,15:45\n"
        "Wielokr. metody oceny i optymalizacji (w) [1],316 S,"
        "2019-10-02,09:50,2019-10-02,11:25,"
        "Fałsz,2020-10-02,09:50\n"
        "Metodyki obiektowe (w) [1],308 S,"
        "2019-10-02,11:40,2019-10-02,13:15,"
        "Fałsz,2019-10-02,11:40\n"
        "Stochastyczne modele eksploatacji SK (w) [1],316 S,"
        "2019-10-04,08:00,2019-10-04,09:35,"
        "Fałsz,2019-10-04,08:00\n"
        "Stochastyczne modele eksploatacji SK (w) [2],316 S,"
        "2019-10-20,09:50,2019-10-04,11:25,"
        "Fałsz,2019-10-04,09:50\n"
        "Analiza strukturalna SI (w) [1],308 S,"
        "2019-10-04,11:40,2019-10-04,13:15,"
        "Fałsz,2019-10-04,11:40\n"
        "Efektywność systemów  informatycznych (w) [1],316 S,"
        "2019-10-04,13:30,2019-10-04,15:05,"
        "Fałsz,2019-10-04,13:30\n"
    )


@pytest.fixture
def longer_schedule_csv_string():
    return (
        "Temat,Lokalizacja,Data rozpoczęcia,"
        "Czas rozpoczęcia,Data zakończenia,Czas zakończenia,"
        "Przypomnienie wł./wył.,Data przypomnienia,Czas przypomnienia\n"
        "Zarządzanie projektami (w) [1],316 S,2019-10-01,15:45,2019-10-01,17:20,"
        "Fałsz,2019-10-01,15:45\n"
        "Wielokr. metody oceny i optymalizacji (w) [1],317 S,"
        "2019-10-02,09:50,2019-10-02,11:25,"
        "Fałsz,2019-10-02,09:50\n"
        "Metodyki obiektowe (w) [1],308 S,"
        "2019-10-02,11:40,2019-10-02,13:15,"
        "Fałsz,2019-10-02,11:40\n"
        "Stochastyczne modele eksploatacji SK (w) [1],316 S,"
        "2019-10-04,08:00,2019-10-04,09:35,"
        "Fałsz,2019-10-04,08:00\n"
        "Stochastyczne modele eksploatacji SK (w) [2],316 S,"
        "2020-10-04,09:50,2019-10-04,11:25,"
        "Fałsz,2019-10-04,09:50\n"
        "Analiza strukturalna SI (w) [1],308 S,"
        "2019-10-04,11:40,2019-10-04,13:15,"
        "Fałsz,2019-10-04,11:40\n"
        "Efektywność systemów  informatycznych (w) [1],316 S,"
        "2019-10-04,13:30,2019-10-04,15:05,"
        "Fałsz,2019-10-04,13:30\n"
        "Ekonomia (w) [1],313 S,"
        "2019-10-04,15:45,2019-10-04,17:20,"
        "Fałsz,2019-10-04,15:45\n"
        "Niezawodność oprogramowania (w) [1],316 S,"
        "2019-10-07,11:40,2019-10-07,13:15,"
        "Fałsz,2019-10-07,11:40\n"
        "Standardy w projektowaniu systemów dialogowych (w) [1],316 S,"
        "2019-10-07,13:30,2019-10-07,15:05,"
        "Fałsz,2019-10-07,13:30\n"
    )


@pytest.fixture
def shorter_timetable(shorter_schedule_csv_string):
    return timetable.Timetable.from_csv_string(shorter_schedule_csv_string)


@pytest.fixture
def longer_timetable(longer_schedule_csv_string):
    return timetable.Timetable.from_csv_string(longer_schedule_csv_string)
