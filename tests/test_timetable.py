"""This module is for testing class and functions from timetable.py module."""


def test_added_event_names_return_names_of_events_present_in_right_parameter_but_not_in_left_parameter(
    shorter_timetable, longer_timetable
):
    result = shorter_timetable.added_event_names(longer_timetable)
    expected = {
        "Ekonomia (w) [1]",
        "Niezawodność oprogramowania (w) [1]",
        "Standardy w projektowaniu systemów dialogowych (w) [1]",
    }
    assert result == expected


def test_removed_event_names_return_names_of_events_present_in_left_parameter_but_not_in_right_parameter(
    shorter_timetable, longer_timetable
):
    result = longer_timetable.removed_event_names(shorter_timetable)
    expected = {
        "Ekonomia (w) [1]",
        "Niezawodność oprogramowania (w) [1]",
        "Standardy w projektowaniu systemów dialogowych (w) [1]",
    }
    assert result == expected


def test_changed_event_names_return_names_of_events_present_in_left_and_right_parameters_but_with_differences_in_fields(
    shorter_timetable, longer_timetable
):
    result = shorter_timetable.changed_event_names(longer_timetable)
    expected = {
        "Wielokr. metody oceny i optymalizacji (w) [1]",
        "Stochastyczne modele eksploatacji SK (w) [2]",
    }
    assert result == expected
